//
//  DroneFactory.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import TwelveDaysGameFramework
import SceneKit

struct DroneEntityFactory: EntityFactory {
    var game: Game

    init(_ game: Game) {
        self.game = game
    }

    func make() -> Entity {
        guard let entityManager = game.entityManager else {
            fatalError(Errors.NO_ENTITY_MANAGER_ERROR)
        }
        guard let sceneNode = game.scene?.rootNode else {
            fatalError(Errors.SCENE_LOADING_ERROR)
        }
        let loader = SceneLoader(GameConstants.DRONE_BODY_SCENE_LABEL)
        let droneNode = loader.get(node: GameConstants.DRONE_ENTITY_LABEL)

        droneNode.position.y = 5
        
        sceneNode.addChildNode(droneNode)
        let entity = Entity(GameConstants.DRONE_ENTITY_LABEL)

        let droneThrusts: [String: Float] = [
            "propeller1": 1.0,
            "propeller2": 1.0,
            "propeller3": 1.0,
            "propeller4": 1.0
        ]

        entityManager.add(component: NodeComponent(node: droneNode), to: entity)
        // TODO: allow different max thrust for different rotors
        entityManager.add(component: RotorComponent(maxThrust: 100.0,
                                                    maxThrustMultiplier: 1.15,
                                                    thrustPerTap: 25.0,
                                                    thrustMultiplierPerTap: 0.02,
                                                    currentThrust: 0,
                                                    currentThrustMultipliers: droneThrusts),
                          to: entity)
        let droneTapObserverManager = ObserverManager(entity)
        droneTapObserverManager.listen(event: DroneTapEvent.self, DroneTapObserver())
        return entity
    }
}
