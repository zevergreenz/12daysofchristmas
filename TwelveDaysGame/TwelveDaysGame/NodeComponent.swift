//
//  NodeComponent.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import SceneKit
import TwelveDaysGameFramework

struct NodeComponent: Component {
    var node: SCNNode
}
