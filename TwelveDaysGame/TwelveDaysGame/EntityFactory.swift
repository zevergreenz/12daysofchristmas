//
//  EntityFactory.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import TwelveDaysGameFramework
import SceneKit

protocol EntityFactory {
    var game: Game { get set }
    func make() -> Entity
}

// - MARK: init
extension EntityFactory {
    /// Initialize the system with an entity manager.
    public init(_ game: Game) {
        self.init(game)
    }
}
