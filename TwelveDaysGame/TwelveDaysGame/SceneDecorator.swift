//
//  SceneDecorator.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import SceneKit

struct SceneDecorator: ViewDecorator {
    private let view: SCNView

    init(_ view: SCNView) {
        self.view = view
    }

    func decorate() {
        let scene = SCNScene()
        view.scene = scene
    }
}
