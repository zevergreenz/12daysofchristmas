//
//  SceneLoader.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import SceneKit

struct SceneLoader {
    var scene: SCNScene
    
    init(_ file: String) {
        guard let scene = SCNScene(named: file) else {
            fatalError(Errors.notifyLoadingErrorForScene(named: file))
        }
        self.scene = scene
    }

    func get(node nodeName: String, _ recursively: Bool = false) -> SCNNode {
        guard let node = scene.rootNode.childNode(withName: nodeName, recursively: recursively) else {
            fatalError(Errors.notifyLoadingErrorForNode(named: nodeName))
        }
        return node
    }
    
    func getNodes() -> [SCNNode] {
        return scene.rootNode.childNodes
    }
}
