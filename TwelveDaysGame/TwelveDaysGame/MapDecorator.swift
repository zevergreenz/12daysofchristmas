//
//  MapDecorator.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import TwelveDaysGameFramework

struct MapDecorator: GameDecorator {
    private let game: Game

    init(_ game: Game) {
        self.game = game
    }

    func decorate() {
        let factory = MapEntityFactory(game)
        _ = factory.make()
    }
}
