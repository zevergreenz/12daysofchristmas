//
//  RotorSystem.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 12/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import TwelveDaysGameFramework
import SceneKit

struct RotorSystem: System {
    internal var entityManager: EntityManager

    private func rotatePropeller(_ droneNode: SCNNode, _ node: SCNNode, _ amount: Float, _ duration: TimeInterval) {
        let action = SCNAction.rotateBy(x: 0, y: CGFloat(amount), z: 0, duration: duration)
        // ease out to smiluate effect of decceleration
        action.timingMode = .easeOut
        node.runAction(action)
    }

    func update(_ time: Double) {
        let droneEntity = entityManager.getEntity(labelled: GameConstants.DRONE_ENTITY_LABEL)

        guard let droneNodeComponent: NodeComponent = droneEntity.getComponent() else {
            fatalError(Errors.notifyLoadingErrorForNode(named: GameConstants.DRONE_ENTITY_LABEL))
        }
        guard var droneRotorComponent: RotorComponent = droneEntity.getComponent() else {
            fatalError(Errors.notifyLoadingErrorForNode(named: GameConstants.DRONE_ENTITY_LABEL))
        }

        let droneNode = droneNodeComponent.node

        for (propellerName, thrustMultiplier) in droneRotorComponent.currentThrustMultipliers {
            guard let propellerNode = droneNode.childNode(withName: propellerName, recursively: false) else {
                continue
            }

            let propellerThrust = thrustMultiplier * droneRotorComponent.currentThrust

            let force: Float = Float(Double(propellerThrust) * time)
            let directedForce = anglesToVector(SCNVector3(x: 0, y: force, z: 0), droneNode.presentation.eulerAngles)

            droneNode.physicsBody?.applyForce(directedForce, at: propellerNode.position, asImpulse: false)

            var newThrust = max(droneRotorComponent.currentThrust
                                    - GameConstants.ROTOR_THRUST_REDUCTION_PER_UNIT_TIME * Float(time),
                                0)
            droneRotorComponent.currentThrust = newThrust

            let newThrustMultiplier = max(thrustMultiplier - 0.005 * Float(time), 1)
            droneRotorComponent.currentThrustMultipliers[propellerName] = newThrustMultiplier

            rotatePropeller(droneNode, propellerNode, Float(time) * 20 * newThrust, time * 20)
        }
        if droneNode.presentation.position.y > GameConstants.DRONE_MAX_FLIGHT_HEIGHT {
            droneNode.position.y = GameConstants.DRONE_MAX_FLIGHT_HEIGHT
        }
        droneEntity.update(component: droneRotorComponent)
    }

    private func anglesToVector(_ vector: SCNVector3, _ angles: SCNVector3) -> SCNVector3 {
        let roll = angles.z
        let pitch = angles.x
        var result = vector

        result.x = result.x - result.y * sin(roll)
        result.y = result.y * cos(roll)

        result.z = result.z + result.y * sin(pitch)
        result.y = result.y * cos(pitch)

        return result
    }
}
