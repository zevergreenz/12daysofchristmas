//
//  PackageComponent.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import TwelveDaysGameFramework

struct PackageComponent: Component {
    var coins: Int
    var timeLimit: Double
    var timeElapsed: Double = 0
    var destination: Entity
}
