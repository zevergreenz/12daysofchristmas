//
//  CameraDecorator.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 10/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import TwelveDaysGameFramework

struct CameraDecorator: GameDecorator {
    private let game: Game

    init(_ game: Game) {
        self.game = game
    }

    func decorate() {
        guard let entityManager = game.entityManager else {
            fatalError(Errors.NO_ENTITY_MANAGER_ERROR)
        }
        let factory = CameraEntityFactory(game)
        let cameraEntity = factory.make()
        
        game.add(system: CameraSystem(entityManager: entityManager))
    }
}
