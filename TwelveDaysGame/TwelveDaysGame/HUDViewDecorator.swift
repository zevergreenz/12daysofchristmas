//
//  HUDViewDecorator.swift
//  TwelveDaysGame
//
//  Created by Nguyen Tuong Van on 14/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import Foundation
import TwelveDaysGameFramework

struct HUDViewDecorator: GameDecorator {
    private let game: Game
    
    init(_ game: Game) {
        self.game = game
    }
    
    func decorate() {
        let skScene = OverlayScene(size: (game.view?.bounds.size)!)
        game.view?.overlaySKScene = skScene
        game.view?.overlaySKScene?.isUserInteractionEnabled = false
        game.hud = skScene
    }
}
