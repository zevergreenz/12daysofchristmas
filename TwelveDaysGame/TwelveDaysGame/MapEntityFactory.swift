//
//  MapEntityFactory.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import TwelveDaysGameFramework
import SceneKit

struct MapEntityFactory: EntityFactory {
    var game: Game

    init(_ game: Game) {
        self.game = game
    }

    func make() -> Entity {
        guard let entityManager = game.entityManager else {
            fatalError(Errors.NO_ENTITY_MANAGER_ERROR)
        }
        guard let sceneNode = game.scene?.rootNode else {
            fatalError(Errors.SCENE_LOADING_ERROR)
        }
        let loader = SceneLoader(GameConstants.MAP_SCENE_LABEL)

        let mapNode = loader.get(node: GameConstants.MAP_ENTITY_LABEL)
        sceneNode.addChildNode(mapNode)
        
        let entity = Entity(GameConstants.MAP_ENTITY_LABEL)
        entityManager.add(component: NodeComponent(node: mapNode), to: entity)
        return entity
    }
}
