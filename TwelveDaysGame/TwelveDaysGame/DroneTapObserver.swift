//
//  DroneTapObserver.swift
//  TwelveDaysGame
//
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import Foundation
import SceneKit
import TwelveDaysGameFramework

public class DroneTapObserver: Observer {
    public func handle(_ object: AnyObject, _ event: Event) {
        // ensure that tap event is a DroneTapEvent
        guard let tapEvent = event as? DroneTapEvent else {
            return
        }

        guard let droneEntity = object as? Entity else {
            return
        }
        guard let droneNodeComponent: NodeComponent = droneEntity.getComponent() else {
            return
        }
        guard var droneRotorComponent: RotorComponent = droneEntity.getComponent() else {
            return
        }

        guard let propellerName = GameConstants.quadrantPropellerMapping[tapEvent.quadrant] else {
            return
        }

        guard var currentThrustMultiplier = droneRotorComponent.currentThrustMultipliers[propellerName] else {
            return
        }

        droneRotorComponent.currentThrust = min(droneRotorComponent.currentThrust + droneRotorComponent.thrustPerTap,
                                                droneRotorComponent.maxThrust)

        currentThrustMultiplier = min(currentThrustMultiplier + droneRotorComponent.thrustMultiplierPerTap,
                                      droneRotorComponent.maxThrustMultiplier)

        droneRotorComponent.currentThrustMultipliers[propellerName] = currentThrustMultiplier
        droneEntity.update(component: droneRotorComponent)
    }
}
