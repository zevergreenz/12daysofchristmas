//
//  DroneTapEvent.swift
//  TwelveDaysGame
//
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import Foundation
import TwelveDaysGameFramework

public class DroneTapEvent: Event {
    enum TapLocation {
        case TOP_LEFT
        case TOP_RIGHT
        case BOTTOM_LEFT
        case BOTTOM_RIGHT
    }

    var quadrant: TapLocation

    init(quadrant: TapLocation) {
        self.quadrant = quadrant
    }
}
