//
//  RotorComponent.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import TwelveDaysGameFramework

struct RotorComponent: Component {
    var maxThrust: Float
    var maxThrustMultiplier: Float
    var thrustPerTap: Float
    var thrustMultiplierPerTap: Float
    var currentThrust: Float = 0
    var currentThrustMultipliers: [String: Float] = [:]
}
