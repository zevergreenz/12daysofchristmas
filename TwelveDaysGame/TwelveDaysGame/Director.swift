//
//  Director.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import UIKit
import SceneKit
import TwelveDaysGameFramework

public class Director {
    public static let tappedEventName = NSNotification.Name.init(rawValue: GameConstants.GAMEVIEW_TAPPED_NOTIF)

    private let game = Game()

    private weak var viewController: UIViewController?

    public init(_ viewController: UIViewController) {
        self.viewController = viewController
    }

    public func setup() {
        guard let viewController = self.viewController else {
            fatalError("View Controller not assigned before calling setup()")
        }

        guard let view = viewController.view as? SCNView else {
            fatalError("View Controller's view is not of SCNView type")
        }

        #if DEBUG
        view.showsStatistics = true
        //view.allowsCameraControl = true
        #endif
        
        game.entityManager = EntityManager()
        game.view = view

        let decorators: [Decorator] = [
            SceneDecorator(view),
            MapDecorator(game),
            HUDViewDecorator(game),
            DroneDecorator(game),
            CameraDecorator(game),
            SunlightDecorator(game),
            DroneHeightDecorator(game)
        ]

        for decorator in decorators {
            decorator.decorate()
        }

        NotificationCenter.default.addObserver(self, selector: #selector(Director.handleTapAction), name: Director.tappedEventName, object: nil)
    }

    public func start() {
        game.start()
    }

    public func stop() {
        game.stop()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
        game.stop()
    }
    
    @objc func handleTapAction(_ notification: NSNotification) {
        guard let location = notification.object as? CGPoint else {
            return
        }
        guard let viewController = self.viewController else {
            print("Tap action handled before view controller was assigned")
            return
        }
        let viewCenter = viewController.view.center
        var quadrant = DroneTapEvent.TapLocation.TOP_LEFT

        if location.y > viewCenter.y {
            if location.x < viewCenter.x {
                quadrant = .BOTTOM_LEFT
            } else {
                quadrant = .BOTTOM_RIGHT
            }
        } else if location.x > viewCenter.x {
            quadrant = .TOP_RIGHT
        }

        let event = DroneTapEvent(quadrant: quadrant)
        game.entityManager?.getEntity(labelled: GameConstants.DRONE_ENTITY_LABEL).notify(event: event)
    }
}

