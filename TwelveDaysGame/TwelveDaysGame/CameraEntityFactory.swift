//
//  CameraEntityFactory.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 10/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import TwelveDaysGameFramework
import SceneKit

struct CameraEntityFactory: EntityFactory {
    var game: Game

    init(_ game: Game) {
        self.game = game
    }

    func make() -> Entity {
        guard let entityManager = game.entityManager else {
            fatalError(Errors.NO_ENTITY_MANAGER_ERROR)
        }
        guard let sceneNode = game.scene?.rootNode else {
            fatalError(Errors.SCENE_LOADING_ERROR)
        }

        let camera = SCNCamera()
        let node = SCNNode()

        node.position = SCNVector3(x: 0, y: GameConstants.TOP_VIEW_CAMERA_HEIGHT, z: 0)
        node.eulerAngles = SCNVector3(x: -Float.pi / 2.0, y: 0, z: 0)

        node.camera = camera

        camera.wantsHDR = true
        camera.aperture = 2.0

        sceneNode.addChildNode(node)
        let entity = Entity(GameConstants.CAMERA_ENTITY_LABEL)
        entityManager.add(component: NodeComponent(node: node), to: entity)
        return entity
    }
}
