//
//  DroneHeightDecorator.swift
//  TwelveDaysGame
//
//  Created by Nguyen Tuong Van on 12/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//
import TwelveDaysGameFramework

struct DroneHeightDecorator: GameDecorator {
    private let game: Game
    
    init(_ game: Game) {
        self.game = game
    }
    
    func decorate() {
        guard let entityManager = game.entityManager else {
            fatalError(Errors.NO_ENTITY_MANAGER_ERROR)
        }
        let factory = DroneHeightEntityFactory(game)
        let droneHeightEntity = factory.make()
        
        game.add(system: DroneHeightSystem(entityManager: entityManager))
    }
}
