//
//  BatteryComponent.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import TwelveDaysGameFramework

struct BatteryComponent: Component {
    var maxCapacity: Int
    var currentCapacity: Int = 0
}
