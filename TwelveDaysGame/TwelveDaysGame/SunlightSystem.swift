//
//  SunlightSystem.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 11/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import TwelveDaysGameFramework
import SceneKit

struct SunlightSystem: System {
    internal var entityManager: EntityManager

    func update(_ time: Double) {
        let droneEntity = entityManager.getEntity(labelled: GameConstants.DRONE_ENTITY_LABEL)
        let sunlightEntity = entityManager.getEntity(labelled: GameConstants.SUNLIGHT_ENTITY_LABEL)

        guard let droneNodeComponent: NodeComponent = droneEntity.getComponent() else {
            fatalError(Errors.notifyLoadingErrorForNode(named: GameConstants.DRONE_ENTITY_LABEL))
        }
        guard let sunlightNodeComponent: NodeComponent = sunlightEntity.getComponent() else {
            fatalError(Errors.notifyLoadingErrorForNode(named: GameConstants.SUNLIGHT_ENTITY_LABEL))
        }

        let droneNode = droneNodeComponent.node.presentation
        let sunlightNode = sunlightNodeComponent.node

        // wanted to make the sunlight follow the drone for time being. We also need to move 
        // the sun over time.
        sunlightNode.position = SCNVector3(x: GameConstants.SUNLIGHT_OFFSET_FROM_DRONE,
                                           y: GameConstants.SUNLIGHT_OFFSET_FROM_DRONE,
                                           z: GameConstants.SUNLIGHT_OFFSET_FROM_DRONE)
    }
}
