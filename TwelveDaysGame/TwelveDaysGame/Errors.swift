//
//  Errors.swift
//  TwelveDaysGame
//
//  Created by lamct on 10/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import Foundation

struct Errors {
    static let NO_ENTITY_MANAGER_ERROR = "Game has no entity manager"
    static let SCENE_LOADING_ERROR = "Scene cannot load"

    static func notifyLoadingErrorForScene(named name: String) -> String {
       return "Unable to load scene \"" + name + "\""
    }
    static func notifyLoadingErrorForNode(named name: String) -> String {
        return "Cannot find node \"" + name + "\""
    }
}
