//
//  OverlayScene.swift
//  TwelveDaysGame
//
//  Created by Nguyen Tuong Van on 13/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//
import UIKit
import SpriteKit

class OverlayScene: SKScene {
    
    override init(size: CGSize) {
        super.init(size: size)
        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
