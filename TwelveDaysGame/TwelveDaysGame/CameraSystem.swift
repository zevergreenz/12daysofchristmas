//
//  CameraSystem.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 10/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import TwelveDaysGameFramework
import SceneKit

struct CameraSystem: System {
    internal var entityManager: EntityManager

    func update(_ time: Double) {
        let droneEntity = entityManager.getEntity(labelled: GameConstants.DRONE_ENTITY_LABEL)
        let cameraEntity = entityManager.getEntity(labelled: GameConstants.CAMERA_ENTITY_LABEL)

        guard let droneNodeComponent: NodeComponent = droneEntity.getComponent() else {
            fatalError(Errors.notifyLoadingErrorForNode(named: GameConstants.DRONE_ENTITY_LABEL))
        }
        guard let cameraNodeComponent: NodeComponent = cameraEntity.getComponent() else {
            fatalError(Errors.notifyLoadingErrorForNode(named: GameConstants.CAMERA_ENTITY_LABEL))
        }

        let droneNode = droneNodeComponent.node.presentation
        let cameraNode = cameraNodeComponent.node

        // TODO: need to get the angle that's normal to plane, not the drone.
        cameraNode.eulerAngles.y = droneNode.presentation.eulerAngles.y

        cameraNode.position = SCNVector3(x: droneNode.position.x, y: cameraNode.position.y, z: droneNode.position.z)
    }
}
