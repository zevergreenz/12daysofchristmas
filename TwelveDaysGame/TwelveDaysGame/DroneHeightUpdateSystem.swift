//
//  DroneHeightUpdateSystem.swift
//  TwelveDaysGame
//
//  Created by Nguyen Tuong Van on 12/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//
import TwelveDaysGameFramework
import SceneKit
import SpriteKit

struct DroneHeightSystem: System {
    internal var entityManager: EntityManager
    
    func update(_ time: Double) {
        let droneEntity = entityManager.getEntity(labelled: GameConstants.DRONE_ENTITY_LABEL)
        let droneHeightEntity = entityManager.getEntity(labelled: GameConstants.DRONE_HEIGHT_ENTITY_LABEL)
        
        guard let droneNodeComponent: NodeComponent = droneEntity.getComponent() else {
            fatalError(Errors.notifyLoadingErrorForNode(named: GameConstants.DRONE_ENTITY_LABEL))
        }
        guard let droneHeightNodeComponent: DroneHeightLabelComponent = droneHeightEntity.getComponent() else {
            fatalError(Errors.notifyLoadingErrorForNode(named: GameConstants.DRONE_HEIGHT_ENTITY_LABEL))
        }

        let droneNode = droneNodeComponent.node.presentation
        let droneHeightNode = droneHeightNodeComponent.node
        
        droneHeightNode.text = String(droneNode.position.y)
    }

}
