//
//  GameConstants.swift
//  TwelveDaysGame
//
//  Created by Nguyen Tuong Van on 10/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//
import Foundation
import UIKit

struct GameConstants {
    static let DRONE_ENTITY_LABEL: String = "drone"
    static let DRONE_BODY_SCENE_LABEL: String = "drone-body.scn"
    static let MAP_ENTITY_LABEL: String = "map"
    static let MAP_SCENE_LABEL: String = "map.scn"
    static let TOP_VIEW_CAMERA_HEIGHT: Float = 25
    static let DRONE_MAX_FLIGHT_HEIGHT: Float = 20
    static let DRONE_HEIGHT_INDICATOR_X_OFFSET: Float = 100
    static let DRONE_HEIGHT_INDICATOR_Y_OFFSET: Float = 100
    static let DRONE_HEIGHT_INDICATOR_FONT_SIZE = 30
    static let DRONE_HEIGHT_INDICATOR_FONT = "DINAlternate-Bold"
    static let DRONE_HEIGHT_INDICATOR_COLOR = UIColor.black
    static let ALTITUDE_LABEL_TEXT: String = "Altitude"
    static let ALTITUDE_LABEL_Y_OFFSET: Float = 50
    static let CAMERA_ENTITY_LABEL: String = "camera"
    static let DRONE_HEIGHT_ENTITY_LABEL: String = "droneHeight"
    static let GAMEVIEW_TAPPED_NOTIF: String = "gameViewTapped"
    static let SUNLIGHT_SCENE_LABEL: String = "sunlight.scn"
    static let SUNLIGHT_ENTITY_LABEL: String = "sunlight"
    static let SUNLIGHT_OFFSET_FROM_DRONE: Float = 25
    static let ROTOR_THRUST_REDUCTION_PER_UNIT_TIME: Float = 32.0
    
    static let quadrantPropellerMapping: [DroneTapEvent.TapLocation: String] = [
        .TOP_LEFT: "propeller1",
        .TOP_RIGHT: "propeller2",
        .BOTTOM_LEFT: "propeller3",
        .BOTTOM_RIGHT: "propeller4"
    ]
}
