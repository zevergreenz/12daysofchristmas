//
//  SunlightEntityFactory.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 11/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import TwelveDaysGameFramework
import SceneKit

struct SunlightEntityFactory: EntityFactory {
    var game: Game

    init(_ game: Game) {
        self.game = game
    }

    func make() -> Entity {
        guard let entityManager = game.entityManager else {
            fatalError(Errors.NO_ENTITY_MANAGER_ERROR)
        }
        guard let sceneNode = game.scene?.rootNode else {
            fatalError(Errors.SCENE_LOADING_ERROR)
        }

        let mapEntity = entityManager.getEntity(labelled: GameConstants.MAP_ENTITY_LABEL)

        guard let mapNode: NodeComponent = mapEntity.getComponent() else {
            fatalError(Errors.notifyLoadingErrorForNode(named: GameConstants.MAP_ENTITY_LABEL))
        }

        let loader = SceneLoader(GameConstants.SUNLIGHT_SCENE_LABEL)
        let sunlightNode = loader.get(node: GameConstants.SUNLIGHT_ENTITY_LABEL)

        guard let floorNode = mapNode.node.childNode(withName: "floor", recursively: true) else {
            fatalError()
        }

        let lookAtConstraint = SCNLookAtConstraint(target: floorNode)
        sunlightNode.constraints = [lookAtConstraint]

        sceneNode.addChildNode(sunlightNode)
        let entity = Entity(GameConstants.SUNLIGHT_ENTITY_LABEL)
        entityManager.add(component: NodeComponent(node: sunlightNode), to: entity)
        return entity

    }
}
