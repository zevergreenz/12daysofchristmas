//
//  SunlightDecorator.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 11/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import TwelveDaysGameFramework

struct SunlightDecorator: GameDecorator {
    private let game: Game

    init(_ game: Game) {
        self.game = game
    }

    func decorate() {
        guard let entityManager = game.entityManager else {
            fatalError(Errors.NO_ENTITY_MANAGER_ERROR)
        }
        let factory = SunlightEntityFactory(game)
        let cameraEntity = factory.make()

        game.add(system: SunlightSystem(entityManager: entityManager))
    }
}
