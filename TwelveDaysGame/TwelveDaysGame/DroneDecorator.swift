//
//  DroneDecorator.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import TwelveDaysGameFramework

struct DroneDecorator: GameDecorator {
    private let game: Game

    init(_ game: Game) {
        self.game = game
    }

    func decorate() {
        guard let entityManager = game.entityManager else {
            fatalError(Errors.NO_ENTITY_MANAGER_ERROR)
        }
        let factory = DroneEntityFactory(game)
        _ = factory.make()

        game.add(system: RotorSystem(entityManager: entityManager))
    }
}
