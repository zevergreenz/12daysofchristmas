//
//  DroneHeightEntityFactory.swift
//  TwelveDaysGame
//
//  Created by Nguyen Tuong Van on 12/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//
import TwelveDaysGameFramework
import SceneKit
import SpriteKit

struct DroneHeightEntityFactory: EntityFactory {
    var game: Game
    
    init(_ game: Game) {
        self.game = game
    }
    
    func make() -> Entity {
        guard let entityManager = game.entityManager else {
            fatalError(Errors.NO_ENTITY_MANAGER_ERROR)
        }
        guard let sceneNode = game.scene?.rootNode else {
            fatalError(Errors.SCENE_LOADING_ERROR)
        }
        guard let overlaySceneNode = game.hud as? OverlayScene else {
            fatalError(Errors.SCENE_LOADING_ERROR)
        }

        let size = overlaySceneNode.size
        let entity = Entity(GameConstants.DRONE_HEIGHT_ENTITY_LABEL)
        var heightIndicatorNode = SKLabelNode(text: "")
        heightIndicatorNode.fontName = GameConstants.DRONE_HEIGHT_INDICATOR_FONT
        heightIndicatorNode.fontColor = GameConstants.DRONE_HEIGHT_INDICATOR_COLOR
        heightIndicatorNode.fontSize = CGFloat(GameConstants.DRONE_HEIGHT_INDICATOR_FONT_SIZE)
        heightIndicatorNode.position = CGPoint(x: size.width - CGFloat(GameConstants.DRONE_HEIGHT_INDICATOR_X_OFFSET), y: CGFloat(GameConstants.DRONE_HEIGHT_INDICATOR_Y_OFFSET))
        overlaySceneNode.addChild(heightIndicatorNode)
        
        // Also add a text to explain this indicates the altitude
        var altitudeNode = SKLabelNode(text: GameConstants.ALTITUDE_LABEL_TEXT)
        altitudeNode.fontName = GameConstants.DRONE_HEIGHT_INDICATOR_FONT
        altitudeNode.fontColor = GameConstants.DRONE_HEIGHT_INDICATOR_COLOR
        altitudeNode.fontSize = CGFloat(GameConstants.DRONE_HEIGHT_INDICATOR_FONT_SIZE) - 4
        altitudeNode.position = CGPoint(x: size.width - CGFloat(GameConstants.DRONE_HEIGHT_INDICATOR_X_OFFSET), y: heightIndicatorNode.position.y + CGFloat(GameConstants.ALTITUDE_LABEL_Y_OFFSET))
        overlaySceneNode.addChild(altitudeNode)
        print(heightIndicatorNode.position.y)
        print(altitudeNode.position.y)
        entityManager.add(component: DroneHeightLabelComponent(node: heightIndicatorNode), to: entity)
        return entity
    }
}
