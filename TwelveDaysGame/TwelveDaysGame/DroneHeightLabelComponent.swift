//
//  DroneHeightLabelComponent.swift
//  TwelveDaysGame
//
//  Created by Nguyen Tuong Van on 13/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//
import SpriteKit
import TwelveDaysGameFramework

struct DroneHeightLabelComponent: Component {
    var node: SKLabelNode
}
