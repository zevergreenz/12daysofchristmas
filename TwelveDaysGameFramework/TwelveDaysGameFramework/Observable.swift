//
//  Observable.swift
//  TwelveDaysGameFramework
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import Foundation

public protocol Observable: class {
    weak var observerManager: ObserverManager? { get set }
}
