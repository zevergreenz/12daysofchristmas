//
//  System.swift
//  TwelveDaysGame
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import Foundation

public protocol System {
    /// The entity manager of the system to create or fetch entities from.
    var entityManager: EntityManager { get set }

    func update(_ time: Double)
}

// - MARK: init
extension System {
    /// Initialize the system with an entity manager.
    public init(entityManager: EntityManager) {
        self.init(entityManager: entityManager)
    }
}
