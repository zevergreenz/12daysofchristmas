//
//  Game.swift
//  TwelveDaysGameFramework
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import Foundation
import SceneKit

public class Game {
    public static let defaultFps = 60

    public let framesPerSecond: Int

    /// All systems registered in the game
    private var _systems: [System] = []

    public var systems: [System] {
        return _systems
    }

    public var entityManager: EntityManager?

    public weak var view: SCNView?
    
    var HUDView: Any?
    
    public var hud: Any? {
        get {
            return HUDView
        }
        set {
            HUDView = newValue
        }
    }

    public var scene: SCNScene? {
        return view?.scene
    }

    private var gameLoop: GameLoop?

    public init(_ fps: Int = Game.defaultFps) {
        self.framesPerSecond = fps
    }

    public func add(system: System) {
        _systems.append(system)
    }

    /// Handles a timer tick in the game loop.
    /// - Parameter delta: The amount of time since last tick.
    private func tick(_ delta: Double) {
        for system in systems {
            system.update(delta)
        }
    }

    /// Start the main game loop.
    public final func start() {
        let gameLoop = GameLoop(fps: framesPerSecond, handler: tick)
        gameLoop.start()
        self.gameLoop = gameLoop
    }

    public final func stop() {
        guard let gameLoop = self.gameLoop else {
            return
        }
        gameLoop.stop()
    }

    deinit {
        stop()
    }
}
