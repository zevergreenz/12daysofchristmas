//
//  EntityManager.swift
//  TwelveDaysGameFramework
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import Foundation

/// Manager of all entities in the game.
public class EntityManager {
    /// Stores all entities organized by the components they have. A single entity may be
    /// referenced more than once by this.
    private var entitiesSortedByComponents: [String: Set<Entity>] = [:]

    private var labelledEntities: [String: Entity] = [:]

    public init() {}

    public func add(labelledEntity entity: Entity) {
        guard let label = entity.label else {
            return
        }
        let currentEntry = labelledEntities[label] as Entity?
        if currentEntry != nil && currentEntry != entity {
            fatalError("Cannot assign two different entities with same label")
        }
        labelledEntities[label] = entity
    }

    /// Add a component to an entity.
    /// - Parameters:
    ///   - component: The component to be added to the entity.
    ///   - entity: The entity to add the component to.
    public func add(component: Component, to entity: Entity) {
        let typeName = String(describing: type(of: component))
        entity.add(component: component)
        var arr: Set<Entity> = entitiesSortedByComponents[typeName] ?? []
        arr.insert(entity)
        entitiesSortedByComponents[typeName] = arr

        add(labelledEntity: entity)
    }

    /// Remove an entity from the world.
    /// - Parameter entity: The entity to be removed.
    public func remove(entity: Entity) {
        for (name, _) in entitiesSortedByComponents {
            _ = entitiesSortedByComponents[name]?.remove(entity)
        }

        if let label = entity.label {
            labelledEntities.removeValue(forKey: label)
        }
    }

    /// Remove a component from an entity.
    /// - Parameters:
    ///   - component: The component to be removed. In effect, only the type of this component is considered.
    ///   - entity: The entity to remove the component from.
    public func remove(component: Component, from entity: Entity) {
        remove(componentType: type(of: component), from: entity)
    }

    /// Remove a component by type from an entity.
    /// - Parameters:
    ///   - componentType: The type of component to remove from the entity.
    ///   - entity: The entity to remove the component from.
    public func remove(componentType: Component.Type, from entity: Entity) {
        let typeName = String(describing: componentType)
        _ = entitiesSortedByComponents[typeName]?.remove(entity)
        entity.remove(componentType: componentType)
    }

    /// Find all entities with a certain component.
    /// - Parameter component: The component that all entities returned must have.
    ///                        In effect, only the type of this component is considered.
    /// - Returns: Returns the list of entities with the component.
    public func findEntities(with component: Component) -> Set<Entity> {
        return findEntities(with: type(of: component))
    }

    /// Find all entities with a certain component.
    /// - Parameter componentType: The component type that all entities returned must have.
    /// - Returns: Returns the list of entities with the component.
    public func findEntities(with componentType: Component.Type) -> Set<Entity> {
        return findEntities(with: [componentType])
    }

    /// Find all entities with a certain component.
    /// - Parameter component: The component that all entities returned must have.
    ///                        In effect, only the type of this component is considered.
    /// - Returns: Returns the list of entities with the component.
    public func findEntities(with components: [Component]) -> Set<Entity> {
        return findEntities(with: components.map( { return type(of: $0) } ))
    }

    /// Find all entities with a certain component.
    /// - Parameter componentType: The component type that all entities returned must have.
    /// - Returns: Returns the list of entities with the component.
    public func findEntities(with componentTypes: [Component.Type]) -> Set<Entity> {
        var result: Set<Entity> = []
        for type in componentTypes {
            let typeName = String(describing: type)
            result = result.union(entitiesSortedByComponents[typeName] ?? [])
        }
        return result
    }

    public func getEntity(labelled label: String) -> Entity {
        guard let entity = labelledEntities[label] else {
            fatalError("Fetching entity with label not found")
        }
        return entity
    }
}
