//
//  GameLoop.swift
//  TwelveDaysGameFramework
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import UIKit

/// Supports game loop operation.
class GameLoop: NSObject {
    var handler: (Double) -> Void
    var displayLink: CADisplayLink?
    var framePerSecond: Int

    /// Initialize the game loop with a frames per second and event handler.
    /// - Parameters:
    ///   - fps: The amount of frames to run per second.
    ///   - handler: The event handler.
    init(fps: Int, handler: @escaping (Double) -> Void) {
        self.handler = handler
        self.framePerSecond = fps
        super.init()
    }

    /// Handles when the timer ticks
    func handleTimer() {
        guard let displayLink = self.displayLink else {
            return
        }
        let delta = displayLink.targetTimestamp - displayLink.timestamp
        handler(delta)
    }

    /// Start the timer
    func start() {
        let displayLink = CADisplayLink(target: self, selector: #selector(self.handleTimer))
        displayLink.preferredFramesPerSecond = framePerSecond
        displayLink.add(to: RunLoop.main, forMode: .commonModes)
        self.displayLink = displayLink
    }

    func stop() {
        guard let displayLink = self.displayLink else {
            return
        }
        displayLink.remove(from: RunLoop.main, forMode: .commonModes)
        displayLink.invalidate()
        self.displayLink = nil
    }

    deinit {
        stop()
    }
}
