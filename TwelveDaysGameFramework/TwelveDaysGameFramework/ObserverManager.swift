//
//  Observer.swift
//  TwelveDaysGameFramework
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import Foundation

public class ObserverManager {
    /// The listeners for events associated with this entity.
    private var observers: [String: [Observer]] = [:]

    private var object: Observable?

    public init(_ object: Observable) {
        self.object = object
        self.object?.observerManager = self
    }

    /// Add a listener to an event on this entity.
    /// - Parameters:
    ///   - event: Name of the event
    ///   - listener: The listener function
    public func listen(event: Event.Type, _ observer: Observer) {
        let eventName = String(describing: event)
        var eventObservers = observers[eventName] ?? []
        eventObservers.append(observer)
        observers[eventName] = eventObservers
    }

    /// Notifies all listeners that an event occurred.
    /// - Parameter event: The name of the event that occurred.
    public func notify(event: Event) {
        guard let object = self.object else {
            return
        }
        let eventName = String(describing: type(of: event))
        let eventObservers = self.observers[eventName] ?? []
        for observer in eventObservers {
            observer.handle(object, event)
        }
    }

    /// Clears all listeners for a particular event.
    /// - Parameter event: The event to clear all listeners.
    public func clearListeners(for event: Event.Type) {
        let eventName = String(describing: event)
        observers.removeValue(forKey: eventName)
    }

    deinit {
        if let object = self.object {
            object.observerManager = nil
        }
    }
}
