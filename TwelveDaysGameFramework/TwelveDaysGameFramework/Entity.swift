//
//  Entity.swift
//  TwelveDaysGameFramework
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import Foundation

public class Entity: Observable, Hashable {
    public var observerManager: ObserverManager?

    private let _label: String?

    public var label: String? {
        return _label
    }

    /// The components of the system mapped by the type of the component.
    private var _components: [String: Component] = [:]

    public init(_ label: String? = nil) {
        self._label = label
    }
    
    public func notify(event: Event) {
        guard observerManager != nil else {
            return
        }
        observerManager!.notify(event: event)
    }

    /// The components of the system mapped by the type of the component.
    public var components: [String: Component] {
        return _components
    }

    /// Add a component to the entity. This should be called from an EntityManager
    /// so that the entity manager registers that this entity holds this component.
    /// - Parameter component: The component to register to this entity.
    func add(component: Component) {
        let typeName = String(describing: type(of: component))
        _components[typeName] = component
    }

    /// Remove a component from this entity. In effect, only the type of this component
    /// is considered for removal.
    /// - Parameter component: The component's type to remove from this entity.
    func remove(component: Component) {
        let typeName = String(describing: type(of: component))
        _components.removeValue(forKey: typeName)
    }

    /// Remove a component from this entity according to the component type.
    /// - Parameter componentType: The component's type to remove from this entity.
    func remove(componentType: Component.Type) {
        let typeName = String(describing: componentType)
        _components.removeValue(forKey: typeName)
    }

    /// Update a component of the entity. Since components are value type, entities
    /// need to be updated with the component after components have been changed.
    /// - Parameter component: The component that was modified.
    public func update(component: Component) {
        let typeName = String(describing: type(of: component))
        guard _components[typeName] != nil else {
            fatalError("Component does not exist in entity " + String(self.hashValue))
        }
        _components[typeName] = component
    }

    /// Get a component according to the type requested.
    /// - Returns: Returns the component if the entity has it, otherwise nil.
    public func getComponent<T: Component>() -> T? {
        let typeName = String(describing: T.self)
        return _components[typeName] as? T
    }

    public var hashValue: Int {
        return ObjectIdentifier(self).hashValue
    }
}

public func ==(_ entityA: Entity, _ entityB: Entity) -> Bool {
    return entityA.hashValue == entityB.hashValue
}
