//
//  Observer.swift
//  TwelveDaysGameFramework
//
//  Created by Sam Yong on 7/3/17.
//  Copyright © 2017 12 Days of Christmas. All rights reserved.
//

import Foundation
import UIKit
import SceneKit

public protocol Observer {
    func handle(_ object: AnyObject, _ event: Event)
}
