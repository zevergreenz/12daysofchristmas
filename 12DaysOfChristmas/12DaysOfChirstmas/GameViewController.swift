//
//  GameViewController.swift
//  12DaysOfChirstmas
//
//  Created by Thanh Lam on 6/3/17.
//  Copyright © 2017 nus.cs3217.a0144881y. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit
import TwelveDaysGame

class GameViewController: UIViewController {

    var director: Director!
    override func viewDidLoad() {
        super.viewDidLoad()
        director = Director(self)
        director.setup()
        director.start()
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self.view)
            NotificationCenter.default.post(name: Director.tappedEventName, object: location)
        }
    }
}
